﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RssService.Model
{
    public class RssData
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public string Language { get; set; }
        public DateTime PubDate { get; set; }

        public List<RssItem> RssItems { get; set; }
    }
}
