﻿using System;

namespace RssService.Model
{
    public class RssItem
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public string Comments { get; set; }
        public string Link { get; set; }
        public DateTime PubDate { get; set; }
        public string Category { get; set; }
        public string Author { get; set; }

        public RssItem()
        {
            Title = "";
            Description = "";
            Comments = "";
            Link = "";
            PubDate = DateTime.Today;
            Category = "";
            Author = "";
        }
    }
}
