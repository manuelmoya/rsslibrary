﻿using System.Threading.Tasks;
using System.Xml;
using System.ServiceModel.Syndication;
using RssService.Model;
using System.Collections.Generic;

namespace RssService.Service
{
    public class RssReader
    {
        private RssData GetRssItems(string url)
        {
            RssData rssData = new RssData();
            try
            {
                XmlReader reader = XmlReader.Create(url);
                SyndicationFeed feed = SyndicationFeed.Load(reader);
                reader.Close();
                rssData.Description = feed.Description.Text;
                rssData.Language = feed.Language;
                rssData.Title = feed.Title.Text;
                rssData.RssItems = new List<RssItem>();
                foreach(SyndicationItem item in feed.Items)
                {
                    RssItem rssItem = new RssItem();
                    rssItem.Title = item.Title.Text;
                    rssItem.PubDate = item.PublishDate.DateTime;
                    if (item.Links != null && item.Links.Count > 0) rssItem.Link = item.Links[0].Uri.ToString(); else rssItem.Link = "No Link";
                    if (item.Authors != null && item.Authors.Count > 0) { rssItem.Author = item.Authors[0].Name.ToString(); } else { rssItem.Author = "Unknown"; }
                    if (item.Categories != null && item.Categories.Count > 0) { rssItem.Category = item.Categories[0].Name.ToString(); } else { rssItem.Category = "No Category"; }
                    rssItem.Description = item.Summary.Text;

                    rssData.RssItems.Add(rssItem);
                }

                return rssData;
            }
            catch
            {
                return rssData;
            }
        }

        public async Task<RssData> GetRssDataAsync(string url)
        {
            return await Task.Run<RssData>(() => this.GetRssItems(url));
        }

        public RssData GetRssItemsSync(string url)
        {
            return this.GetRssItems(url);

        }
    }
}
