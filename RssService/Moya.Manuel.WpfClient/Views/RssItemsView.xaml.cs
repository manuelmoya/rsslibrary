﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using Moya.Manuel.WpfClient.ViewModels;

namespace Moya.Manuel.WpfClient.Views
{
    /// <summary>
    /// Interaction logic for RssItemsView.xaml
    /// </summary>
    public partial class RssItemsView : UserControl
    {
        private RssItemsViewModel _vm;

        public RssItemsView()
        {
            InitializeComponent();

            _vm = new RssItemsViewModel();
            this.DataContext = _vm;

            _vm.LoadItems();

        }
    }
}
