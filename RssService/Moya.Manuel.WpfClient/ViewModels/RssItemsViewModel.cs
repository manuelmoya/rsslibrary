﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RssService.Model;
using RssService.Service;
using System.Collections.ObjectModel;

namespace Moya.Manuel.WpfClient.ViewModels
{
    public class RssItemsViewModel : ViewModelBase
    {
        #region Properties
        private ObservableCollection<RssItem> _items = new ObservableCollection<RssItem>();
        private RssItem _selectedItem = default(RssItem);
        private RssData _rssData = default(RssData);

        public ObservableCollection<RssItem> Items
        {
            get => _items;
            set => Set(ref _items, value);
        }

        public RssItem SelectedItem
        {
            get => _selectedItem;
            set => Set(ref _selectedItem, value);
        }

        public RssData rssData
        {
            get => _rssData;
            set => Set(ref _rssData, value);
        }

        #endregion
        #region Methods
        public async void LoadItems()
        {
            RssReader reader = new RssReader();
            RssData rssData = new RssData();
            this.rssData = await reader.GetRssDataAsync("https://channel9.msdn.com/feeds/rss");

            this.Items = new ObservableCollection<RssItem>(this.rssData.RssItems.OrderByDescending(r => r.PubDate));

            if (this.Items != null && this.Items.Count > 0) this.SelectedItem = this.Items[0];

        }
        #endregion


    }
}
