﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using RssService.Model;
using RssService.Service;

namespace Moya.Roldan.WebMvc.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            RssReader reader = new RssReader();
            RssData rssData = new RssData();
            rssData = reader.GetRssItemsSync("https://channel9.msdn.com/feeds/rss");

            ViewBag.RSSFeed = rssData;

            return View();
        }

    }
}